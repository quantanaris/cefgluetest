﻿using Xilium.CefGlue;

namespace CefglueTest {
	public class DemoCefClient : CefClient {
		readonly private DemoCefLoadHandler _loadHandler;
		readonly private DemoCefRenderHandler _renderHandler;

		public DemoCefClient(int windowWidth, int windowHeight) {
			_renderHandler = new DemoCefRenderHandler(windowWidth, windowHeight);
			_loadHandler = new DemoCefLoadHandler();
		}

		protected override CefRenderHandler GetRenderHandler() {
			return _renderHandler;
		}

		protected override CefLoadHandler GetLoadHandler() {
			return _loadHandler;
		}
	}
}