﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using Xilium.CefGlue;

namespace CefglueTest {
	public class DemoCefRenderHandler : CefRenderHandler {
		protected readonly int windowHeight;
		protected readonly int windowWidth;

		public DemoCefRenderHandler(int windowWidth, int windowHeight) {
			this.windowWidth = windowWidth;
			this.windowHeight = windowHeight;
		}

		protected override CefAccessibilityHandler GetAccessibilityHandler() {
			return null;
		}

		protected override bool GetRootScreenRect(CefBrowser browser, ref CefRectangle rect) {
			GetViewRect(browser, out rect);
			return true;
		}

		protected override void GetViewRect(CefBrowser browser, out CefRectangle rect) {
			rect = new CefRectangle(0, 0, windowWidth, windowHeight);
			// rect.X = 0;
			// rect.Y = 0;
			// rect.Width = _windowWidth;
			// rect.Height = _windowHeight;
		}

		protected override bool GetScreenPoint(
			CefBrowser browser, int viewX, int viewY, ref int screenX,
			ref int screenY
		) {
			screenX = viewX;
			screenY = viewY;
			return true;
		}

		protected override bool GetScreenInfo(CefBrowser browser, CefScreenInfo screenInfo) {
			return false;
		}

		protected override void OnPopupSize(CefBrowser browser, CefRectangle rect) {
		}

		protected override void OnPaint(
			CefBrowser browser,
			CefPaintElementType type,
			CefRectangle[] dirtyRects,
			IntPtr buffer,
			int width,
			int height
		) {
			// Save the provided buffer (a bitmap image) as a PNG.
			var bitmap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppArgb, buffer);
			bitmap.Save("LastOnPaint.png", ImageFormat.Png);
		}

		protected override void OnAcceleratedPaint(CefBrowser browser, CefPaintElementType type,
			CefRectangle[] dirtyRects, IntPtr sharedHandle) {
			throw new NotImplementedException();
		}

		protected override void OnScrollOffsetChanged(CefBrowser browser, double x, double y) {
			throw new NotImplementedException();
		}

		protected override void OnImeCompositionRangeChanged(CefBrowser browser, CefRange selectedRange,
			CefRectangle[] characterBounds) {
			throw new NotImplementedException();
		}

		// protected override void OnCursorChange(CefBrowser browser, IntPtr cursorHandle) {
		// }
		//
		// protected override void OnScrollOffsetChanged(CefBrowser browser) {
		// }
	}
}