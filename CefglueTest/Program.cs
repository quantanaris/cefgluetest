﻿using System;
using System.IO;
using Xilium.CefGlue;

namespace CefglueTest {
	class Program {
		[STAThread]
		static int Main(string[] args) {
			// Load CEF. This checks for the correct CEF version.
			Console.WriteLine("Loading CEF");
			CefRuntime.Load();
			Console.WriteLine("CEF Loaded");
			var mainArgs = new CefMainArgs(Array.Empty<string>());
			var cefApp = new DemoCefApp();

			var exitCode = CefRuntime.ExecuteProcess(mainArgs, cefApp, IntPtr.Zero);
			if (exitCode != -1) { return exitCode; }


			// Start the secondary CEF process.
			// var cefMainArgs = new CefMainArgs(new string[0]);
			// var cefApp = new DemoCefApp();

			// This is where the code path divereges for child processes.
			// if (CefRuntime.ExecuteProcess(cefMainArgs, cefApp, IntPtr.Zero) != -1) {
			// 	Console.Error.WriteLine("Could not the secondary process.");
			// }

			// Settings for all of CEF (e.g. process management and control).
			var browserSubprocessPath = Path.Combine(
				Directory.GetCurrentDirectory(),
				"NURenderer.exe"
			);
			Console.WriteLine(browserSubprocessPath + "   " + File.Exists(browserSubprocessPath));
			var cefSettings = new CefSettings {
				BrowserSubprocessPath = browserSubprocessPath,
				WindowlessRenderingEnabled = true,
				MultiThreadedMessageLoop = true,
				LogSeverity = CefLogSeverity.Error,
				LogFile = "cef.log",
			};
			Console.WriteLine("Init browser");
			// Start the browser process (a child process).
			CefRuntime.Initialize(mainArgs, cefSettings, cefApp, IntPtr.Zero);

			// Instruct CEF to not render to a window at all.
			var cefWindowInfo = CefWindowInfo.Create();
			cefWindowInfo.SetAsWindowless(IntPtr.Zero, true); // .SetAsOffScreen(IntPtr.Zero);
			cefWindowInfo.WindowlessRenderingEnabled = true;

			// Settings for the browser window itself (e.g. enable JavaScript?).
			var cefBrowserSettings = new CefBrowserSettings();

			// Initialize some the cust interactions with the browser process.
			// The browser window will be 1280 x 720 (pixels).
			var cefClient = new DemoCefClient(1280, 720);

			// Start up the browser instance.
			CefBrowserHost.CreateBrowser(
				cefWindowInfo,
				cefClient,
				cefBrowserSettings,
				"https://www.google.com/"
			);

			//var host = browser.GetHost();

			// var wi = CefWindowInfo.Create();
			// wi.SetAsPopup(IntPtr.Zero, "DevTools");
			//
			// host.ShowDevTools(wi, new DevToolsWebClient(), new CefBrowserSettings(), new CefPoint(0, 0));
			//OpenDeveloperToolsCommand(browser);

			// Hang, to let the browser to do its work.
			Console.WriteLine("Press a key at any time to end the program.");
			Console.ReadKey();

			// Clean up CEF.
			CefRuntime.Shutdown();

			return 0;
		}

		private class DevToolsWebClient : CefClient {
		}
	}
}