﻿using System;
using Xilium.CefGlue;

namespace CefglueTest {
	class Program {
		static int Main(string[] args) {
			// Load CEF. This checks for the correct CEF version.
			//CefRuntime.Load();

			var mainArgs = new CefMainArgs(args);
			var cefApp = new DemoApp();

			var exitCode = CefRuntime.ExecuteProcess(mainArgs, cefApp, IntPtr.Zero);
			if (exitCode != -1) {
				Console.WriteLine($"Unexpected error {exitCode}");
				return exitCode;
			}

			// var cefSettings = new CefSettings {
			// 	WindowlessRenderingEnabled = true,
			// 	MultiThreadedMessageLoop = true,
			// 	LogSeverity = CefLogSeverity.Verbose,
			// 	LogFile = "cef.log",
			// };

			Console.WriteLine($"Sub process exit {exitCode}");
			//CefRuntime.Initialize(mainArgs, cefSettings, cefApp, IntPtr.Zero);
			CefRuntime.Shutdown();
			return exitCode;
		}
	}

	internal sealed class DemoApp : CefApp {
		protected override void OnBeforeCommandLineProcessing(string processType, CefCommandLine commandLine) {
			Console.WriteLine("SUB PROC");
		}
	}
}